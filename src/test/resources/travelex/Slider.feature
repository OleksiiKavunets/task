Feature: User can swipe a slider

  Scenario: User opens Travelex page and swipes left a slider two times
    User verifies slider name

    Given User opens Travelex page
    When User swipes left a slider two times
    Then User should see a slider with name "Buy foreign currency"