package stepdefs;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import domain.pages.travelex.TravelexPage;
import org.testng.Assert;

public class TravelexPageSteps {
    TravelexPage page = new TravelexPage();

    @Given("User opens Travelex page")
    public void userOpensTravelexPage(){
        page.open();
    }

    @When("User swipes left a slider two times")
    public void userSwipesLeftSlider(){
        page.swipeLeft().swipeLeft();
    }

    @Then("User should see a slider with name \"(.*)\"")
    public void userShouldSeeSliderWithName(String name){
        Assert.assertEquals(page.getSliderName(), name, "INCORRECT NAME OF DISPLAYED SLIDER");
    }
}
