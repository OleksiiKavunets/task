package domain.pages.travelex;

import com.codeborne.selenide.Configuration;
import com.codeborne.selenide.Selenide;
import com.codeborne.selenide.SelenideElement;
import org.openqa.selenium.Keys;

import static com.codeborne.selenide.Selenide.$;

public class TravelexPage {

    private SelenideElement slider = $("[class*=\"slick-slide slick-active\"]");
    private SelenideElement sliderName = $(".inner-wrap a[href=\"/currency\"]");

    public void open() {
        Configuration.browserSize = "550x800";
        Selenide.open("https://www.travelex.co.uk");
    }

    public TravelexPage swipeLeft() {
        slider.sendKeys(Keys.ARROW_RIGHT);
        return this;
    }

    public String getSliderName() {
        return sliderName.text();
    }
}
