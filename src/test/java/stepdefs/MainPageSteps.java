package stepdefs;

import com.codeborne.selenide.Selenide;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import domain.pages.wiki.ArticlePage;
import domain.pages.wiki.MainPage;
import org.testng.Assert;

public class MainPageSteps {

    MainPage page = new MainPage();

    private String searchContext;

    @Given("User opens Wiki page")
    public void userOpensWikiPage() {
        page.open();
    }

    @Then("User checks the title of page to be \"(.*)\"")
    public void userChecksTitle(String pageTitle) {
        Assert.assertEquals(Selenide.title(), pageTitle,
                "INCORRECT TITLE OF CURRENT PAGE");
    }

    @When("User enters text \"(.*)\" to search field")
    public void userEntersTextToSearchField(String text) {
        page.enterSearchText(text);
        searchContext = text;
    }

    @Then("Wiki should show \"(.*)\" suggestions")
    public void wikiShouldShowAmountOfSuggestions(String amount) {
        Assert.assertTrue(page.getSuggestions().size() >= Integer.valueOf(amount),
                "WIKI SHOWED AMOUNT OF SUGGESTIONS LESS THAN " + amount + " FOR SEARCH '" + searchContext + "'");
    }

    @And("Wiki suggestions should contain \"(.*)\"")
    public void wikiSuggestionsShouldContain(String text){
        Assert.assertTrue(page.verifySuggestionsToContainSearchContext(text));
    }

    @When("User selects suggestion (\\d+)")
    public void userSelectSuggestion(Integer s){
        page.getSuggestions().get(s - 1).click();
        new ArticlePage().waitForPageToOpen();
    }


}
