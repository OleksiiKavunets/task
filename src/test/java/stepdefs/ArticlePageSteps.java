package stepdefs;

import cucumber.api.java.en.And;
import cucumber.api.java.en.Then;
import domain.pages.wiki.ArticlePage;
import org.testng.Assert;

public class ArticlePageSteps {
    ArticlePage page = new ArticlePage();

    @Then("Article page should have an article title")
    public void articlePageShouldHaveTitle() {
        Assert.assertTrue(page.isTitlePresent(), "TITLE OF ARTICLE IS NOT DISPLAYED");
    }

    @And("Article page should have a contents table")
    public void articlePageShouldHaveContents(){
        Assert.assertTrue(page.isTableOfContentsPresent(), "TABLE OF CONTENTS IS NOT DISPLAYED");
    }


}
