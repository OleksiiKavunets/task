package domain.pages.wiki;

import com.codeborne.selenide.Configuration;
import com.codeborne.selenide.Selenide;
import com.codeborne.selenide.SelenideElement;
import com.codeborne.selenide.WebDriverRunner;
import org.openqa.selenium.TimeoutException;
import org.openqa.selenium.support.ui.ExpectedConditions;
import utils.TextUtil;

import java.time.Duration;
import java.util.List;

import static com.codeborne.selenide.Selenide.$;
import static com.codeborne.selenide.Selenide.$$;
import static com.codeborne.selenide.Selenide.Wait;

public class MainPage {

    private SelenideElement searchInput = $("[id=\"searchInput\"]");
    private SelenideElement submitBtn = $("[type=\"submit\"]");
    private SelenideElement suggestion = $("[id=\"typeahead-suggestions\"] *");

    public void open() {
        WebDriverRunner.getWebDriver().manage().window().maximize();
        Selenide.open("http://www.wikipedia.org");
    }

    public void search(String s) {
        searchInput.val(s);
        submitBtn.click();
    }

    public MainPage enterSearchText(String text) {
        searchInput.val(text);

        return this;
    }

    public List<SelenideElement> getSuggestions() {
        try {
            Wait().withTimeout(Duration.ofSeconds(10)).until(ExpectedConditions.visibilityOf(suggestion));
        } catch (TimeoutException e) {
            System.out.println();
        }
        return $$("[id=\"typeahead-suggestions\"] *");
    }

    public boolean verifySuggestionsToContainSearchContext(String searchContext) {
        boolean contains = false;
        for (String s : TextUtil.textOf(getSuggestions())) {
            if (s.toLowerCase().contains(searchContext)) {
                contains = true;
                break;
            }
        }
        return contains;
    }
}
