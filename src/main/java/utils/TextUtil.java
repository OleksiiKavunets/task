package utils;

import com.codeborne.selenide.SelenideElement;
import org.openqa.selenium.WebElement;

import java.util.List;
import java.util.stream.Collectors;

public class TextUtil {

    public static List<String> textOf(List<SelenideElement> list){
        return list.stream().map(s -> s.getText()).collect(Collectors.toList());
    }
}
