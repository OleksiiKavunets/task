Feature: Wiki shows suggestions according to user search

  Scenario Outline: User opens Wiki page and types text to search field
  User checks amount of suggestions to be no less than 20

    Given User opens Wiki page
    When User enters text "<text>" to search field
    Then Wiki should show "<amount>" suggestions

    Examples:
      | text          | amount |
      | furry rabbits | 20     |
      | rabbit        | 20     |


  Scenario Outline: User opens Wiki page and types text to search field
  User checks suggestions to have search context

    Given User opens Wiki page
    When User enters text "<text>" to search field
    Then Wiki suggestions should contain "<text>"
    Examples:
      | text   |
      | rabbit |

