package domain.pages.wiki;

import com.codeborne.selenide.SelenideElement;
import org.openqa.selenium.support.ui.ExpectedConditions;

import static com.codeborne.selenide.Selenide.$;
import static com.codeborne.selenide.Selenide.Wait;

public class ArticlePage {

    private SelenideElement title = $("#firstHeading");
    private SelenideElement tableOfContents = $("[id=toc]");
    private SelenideElement menu = $("[id=\"p-navigation\"]");

    public boolean isTitlePresent() {
        return title.exists();
    }

    public boolean isTableOfContentsPresent() {
        return tableOfContents.exists();
    }

    public void waitForPageToOpen() {
        Wait().until(ExpectedConditions.visibilityOf(menu));
    }
}
