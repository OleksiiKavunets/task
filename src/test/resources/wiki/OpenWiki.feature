Feature: Wiki page should open successfully

  Scenario: User opens Wiki page and checks the title of the page

    Given User opens Wiki page
    Then User checks the title of page to be "Wikipedia"