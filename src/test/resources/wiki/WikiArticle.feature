Feature: Wiki article should contain an article title and a table of contents

  Scenario: User opens Wiki page and types text to search field
    User selects first suggestion and opens an Article page
    User verifies that the Article page has an article title and a table of contents

    Given User opens Wiki page
    And User enters text "budda" to search field
    When User selects suggestion 1
    Then Article page should have an article title
    And Article page should have a contents table

